<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>themes/blackness/main.css" >
</head>
<body>

<div id="main_win">
    <div class="title"></div>

    <form action="<?php echo base_url(); ?>index.php/DoLogin" method="POST" enctype="multipart/form-data">
        <div class="form bound" style="margin-top: 40px;">
            <table>
                <tbody>
                    <tr>
                        <td>Email:</td>
                        <td><input name="txtEmail" type="text" class="field" /></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input name="txtPassword" type="password" class="field" /></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <button id="btnLogin" class="button">Login</button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </form>
</div>

</body>
</html>
